//
//  SecondViewController.m
//  Nav_Bar_ObjC_iPhone_Pata
//
//  Created by Bruno Tavares on 20/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "SecondViewController.h"

NSUInteger const kLabelHeight = 300;
NSUInteger const kLabelWidth = 70;

@interface SecondViewController ()

@property (nonatomic, strong) UILabel *label;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.label = [[UILabel alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width / 2 - kLabelWidth / 2, [[UIScreen mainScreen] bounds].size.height / 2 - kLabelHeight / 2, kLabelWidth, kLabelHeight)];
    
    self.label.font = [UIFont boldSystemFontOfSize:90.0f];
    self.label.text = @"2";
    
    [self.view addSubview:self.label];
    
    self.view.backgroundColor = [UIColor whiteColor];
}

@end
