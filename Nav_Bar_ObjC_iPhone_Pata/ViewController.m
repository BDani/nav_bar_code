//
//  ViewController.m
//  Nav_Bar_ObjC_iPhone_Pata
//
//  Created by Bruno Tavares on 20/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"

NSUInteger const kButtonWidth = 200;
NSUInteger const kButtonHeight = 30;

@interface ViewController ()

@property (nonatomic, strong) UIButton *button;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"View 1";
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.button = [[UIButton alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width / 2 - kButtonWidth / 2, [[UIScreen mainScreen] bounds].size.height / 2 - kButtonHeight / 2, kButtonWidth, kButtonHeight)];
    
    self.button.titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    
    [self.button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [self.button setTitle:@"Go To View 2" forState:UIControlStateNormal];
    [self.view addSubview:self.button];
    
    [self.button addTarget:self action:@selector(changeView:) forControlEvents:UIControlEventTouchDown];
}

- (void)changeView: (id) sender {
    
    SecondViewController *viewController2 = [[SecondViewController alloc] init];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:viewController2];
    
    viewController2.title = @"View 2";
    
    [self.navigationController pushViewController:viewController2 animated:YES];
};

@end
